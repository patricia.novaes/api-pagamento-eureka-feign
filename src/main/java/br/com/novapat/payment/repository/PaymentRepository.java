package br.com.novapat.payment.repository;


import br.com.novapat.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findAllByCreditCardId(Long creditCardId);
}

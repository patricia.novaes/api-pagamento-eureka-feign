package br.com.novapat.payment.controller;


import br.com.novapat.payment.models.Payment;
import br.com.novapat.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/pagamento")
    public Payment create(@RequestBody Payment payment) {
        return paymentService.create(payment);
    }

    @GetMapping("/pagamentos/{creditCardId}")
    public List<Payment> findAllByCreditCard(@PathVariable Long creditCardId) {
        return paymentService.findAllByCreditCard(creditCardId);
    }

}

package br.com.novapat.payment.service;


import br.com.novapat.payment.clients.CreditCardClient;
import br.com.novapat.payment.models.Payment;
import br.com.novapat.payment.models.dto.CreditCardResponse;
import br.com.novapat.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditCardClient creditCardClient;

    public Payment create(Payment payment) {
        CreditCardResponse creditCardResponse = creditCardClient.getCartaoById(payment.getCreditCardId());
        payment.setCreditCardId(creditCardResponse.getId());
        return paymentRepository.save(payment);
    }

    public List<Payment> findAllByCreditCard(Long creditCardId) {
        CreditCardResponse creditCardResponse = creditCardClient.getCartaoById(creditCardId);
        return paymentRepository.findAllByCreditCardId(creditCardResponse.getId());
    }

}

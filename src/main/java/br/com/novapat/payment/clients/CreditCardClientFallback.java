package br.com.novapat.payment.clients;

import br.com.novapat.creditcard.clients.CustomerClient;
import br.com.novapat.creditcard.model.dto.CustomerDTO;
import br.com.novapat.payment.models.dto.CreditCardResponse;

public class CreditCardClientFallback implements CreditCardClient {

    @Override
    public CreditCardResponse getCartaoById(Long id) {
        throw new CreditCardOfflineException();
    }
}

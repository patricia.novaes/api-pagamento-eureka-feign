package br.com.novapat.payment.clients;

import br.com.novapat.creditcard.clients.CustomerClientConfiguration;
import br.com.novapat.payment.models.dto.CreditCardResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CreditCardClientConfiguration.class)
public interface CreditCardClient {

        @GetMapping("v1/cartao/id/{id}")
        CreditCardResponse getCartaoById(@PathVariable Long id);

}

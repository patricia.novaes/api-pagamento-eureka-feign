package br.com.novapat.payment.clients;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "O sistema de cartão está offline!")
public class CreditCardOfflineException extends RuntimeException {
}

package br.com.novapat.payment.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O pagamento informado é inválido")
public class InvalidPaymentException extends RuntimeException {
}

package br.com.novapat.payment.clients;

import br.com.novapat.creditcard.clients.InvalidCustomerException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CreditCardClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new InvalidPaymentException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
